import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Demo';
  greeting = {};
  constructor(private http: HttpClient) {
    http.get('https://hello-service-1qv1.onrender.com/api/v1', {responseType: 'text'})
      .subscribe(data => this.greeting = data);
  }
}
